# coding=utf-8
"""
This file is your main submission that will be graded against. Only copy-paste
code on the relevant classes included here. Do not add any classes or functions
to this file that are not part of the classes that we want.
"""

import heapq
import math


# References:  Bi Directional A Star - Slides.pdf
#             Search Algorithms Slide Deck

class PriorityQueue(object):
    """
    A queue structure where each element is served in order of priority.

    Elements in the queue are popped based on the priority with higher priority
    elements being served before lower priority elements.  If two elements have
    the same priority, they will be served in the order they were added to the
    queue.

    Traditionally priority queues are implemented with heaps, but there are any
    number of implementation options.

    (Hint: take a look at the module heapq)

    Attributes:
        queue (list): Nodes added to the priority queue.
    """

    def __init__(self):
        """Initialize a new Priority Queue."""

        self.queue = list()
        self.cnter = 0

    def pop(self):
        """
        Pop top priority node from queue.

        Returns:
            The node with the highest priority.
        """
        # TODO
        return heapq.heappop(self.queue)[-1]

    def remove(self, node):
        """
        Remove a node from the queue.

        Hint: You might require this in ucs. However, you may
        choose not to use it or to define your own method.

        Args:
            node (tuple): The node to remove from the queue.
        """
        self.queue.pop()
        heapq.heapify(self.queue)

    def __iter__(self):
        """Queue iterator."""

        return iter(sorted(self.queue))

    def __str__(self):
        """Priority Queue to string."""

        return 'PQ:%s' % self.queue

    def append(self, node):
        """
        Append a node to the queue.

        Args:
            node: Comparable Object to be added to the priority queue.
        """

        heapq.heappush(self.queue, (node[0], self.cnter, node))
        self.cnter += 1

    def __contains__(self, key):
        """
        Containment Check operator for 'in'

        Args:
            key: The key to check for in the queue.

        Returns:
            True if key is found in queue, False otherwise.
        """

        return key in [n[-1] for n in self.queue]

    def __eq__(self, other):
        """
        Compare this Priority Queue with another Priority Queue.

        Args:
            other (PriorityQueue): Priority Queue to compare against.

        Returns:
            True if the two priority queues are equivalent.
        """

        return self.queue == other.queue

    def size(self):
        """
        Get the current size of the queue.

        Returns:
            Integer of number of items in queue.
        """

        return len(self.queue)

    def clear(self):
        """Reset queue to empty (no nodes)."""

        self.queue = list()

    def top(self):
        """
        Get the top item in the queue.

        Returns:
            The first item stored in teh queue.
        """

        return self.queue[0]

    def peek(self):
        if self.queue[0][0]:
            return self.queue[0][0]


def goal_test(start, goal, graph):
    """
    Check for early terminate conditions
    """

    if start == goal:
        return True

    return False


def is_node_found(start, goal, graph):
    """
    Checks whether start or goal nodes are in in the explored set
    """

    if start not in graph.explored_nodes.keys() or goal not in graph.explored_nodes.keys():
        return True

    return False


def breadth_first_search(graph, start, goal):
    """
    Warm-up exercise: Implement breadth-first-search.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    explored = list()
    frontier = PriorityQueue()
    path_cost = 0

    if start == goal:
        return list()

    frontier.append((path_cost, start))
    while frontier:
        cost, path = frontier.pop()
        node = path[-1]

        frontiers = list()
        for f in frontier.queue:
            for x in f[2]:
                frontiers.append(x)

        neighbors = list()
        if node not in explored and node not in frontiers:
            neighbors = sorted(graph.neighbors(node))
        explored.append(node)

        for c in neighbors:
            if c not in explored and c not in frontiers:
                followed_path = list(path)
                followed_path.append(c)
                path_cost = len(followed_path) - 1
                if goal_test(c, goal, graph):
                    return followed_path
                frontier.append((path_cost, followed_path))

    return list()


def uniform_cost_search(graph, start, goal):
    """
    Warm-up exercise: Implement uniform_cost_search.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    if goal_test(start, goal, graph):
        return list()

    if is_node_found(start, goal, graph):
        return list()

    frontier = PriorityQueue()
    explored = dict()
    explored_path_costs = dict()

    frontier.append((0, start))
    explored[start] = [start]
    explored_path_costs[start] = 0

    while frontier:
        path_cost, start = frontier.pop()  # lowest-cost node in frontier
        if goal_test(start, goal, graph):
            return explored[goal]
        for n in graph.neighbors(start):
            # if child not in explored or frontier
            if n not in explored or n not in frontier:
                # insert child onto frontier
                # cost to reach the node
                g = path_cost + graph.get_edge_weight(start, n)
                if explored_path_costs.get(n, float('inf')) > g:
                    # if child is in frontier with higher path-cost
                    # then replace that frontier node with child
                    frontier.append((g, n))
                    explored[n] = explored[start] + [n]
                    explored_path_costs[n] = g

    return list()


def null_heuristic(graph, v, goal):
    """
    Null heuristic used as a base line.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        v (str): Key for the node to calculate from.
        goal (str): Key for the end node to calculate to.

    Returns:
        0
    """

    return 0


def euclidean_dist_heuristic(graph, v, goal):
    """
    Warm-up exercise: Implement the euclidean distance heuristic.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        v (str): Key for the node to calculate from.
        goal (str): Key for the end node to calculate to.

    Returns:
        Euclidean distance between `v` node and `goal` node
    """

    node_x, node_y = graph.nodes[v]['pos']
    goal_x, goal_y = graph.nodes[goal]['pos']

    return math.sqrt(math.pow(node_x - goal_x, 2) + math.pow(node_y - goal_y, 2))


def a_star(graph, start, goal, heuristic=euclidean_dist_heuristic):
    """
    Warm-up exercise: Implement A* algorithm.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    if goal_test(start, goal, graph):
        return list()

    if is_node_found(start, goal, graph):
        return list()

    frontier = PriorityQueue()
    explored = dict()
    explored_path_costs = dict()

    frontier.append((0, 0, start))
    explored[start] = [start]
    explored_path_costs[start] = 0

    while frontier:
        # lowest-cost node in frontier
        _, path_cost, start = frontier.pop()
        if goal_test(start, goal, graph):
            return explored[goal]
        for n in graph.neighbors(start):
            if n not in explored or n not in frontier:
                # cost to reach the node
                g = path_cost + graph.get_edge_weight(start, n)
                # cost to get from node to goal
                h = heuristic(graph, n, goal)
                f = g + h  # estimated cost of the cheapest solution through n
                if explored_path_costs.get(n, float('inf')) > f:
                    # if child is in frontier with higher path-cost
                    # then replace that frontier node with child
                    frontier.append((f, g, n))
                    explored[n] = explored[start] + [n]
                    explored_path_costs[n] = f

    return list()


def uni_search(frontier, explored_set, explored, best_cost, goal_explored, start_explored_path_costs,
               goal_explored_path_costs, graph, path_intersection):
    node_cost, node = frontier.pop()
    explored_set.add(node)
    if node in goal_explored.keys() and start_explored_path_costs[node] + goal_explored_path_costs[
        node] < best_cost:
        path_intersection = explored[node] + [node] + goal_explored[node]
        best_cost = start_explored_path_costs[node] + goal_explored_path_costs[node]
    for n in graph.neighbors(node):
        if n not in explored_set or n not in frontier:
            cost = graph.get_edge_weight(node, n) + node_cost
            if start_explored_path_costs.get(n, float('inf')) > cost:
                frontier.append((cost, n))
                explored[n] = explored[node] + [node]
                start_explored_path_costs[n] = cost

    return path_intersection, best_cost, frontier, explored, start_explored_path_costs, explored_set


def bidirectional_ucs(graph, start, goal):
    """
    Exercise 1: Bidirectional Search.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    if goal_test(start, goal, graph):
        return list()

    if is_node_found(start, goal, graph):
        return list()

    start_frontier = PriorityQueue()
    goal_frontier = PriorityQueue()
    start_frontier.append((0, start))
    goal_frontier.append((0, goal))

    start_explored = {start: list()}
    goal_explored = {goal: list()}
    start_explored_path_costs = {start: 0}
    goal_explored_path_costs = {goal: 0}

    start_explored_set = set()
    goal_explored_set = set()

    best_cost = float('inf')
    path_intersection = list()

    while start_frontier and goal_frontier:

        # search forward
        start_cost, start_node = start_frontier.pop()
        start_explored_set.add(start_node)
        if start_node in goal_explored.keys() and start_explored_path_costs[start_node] + goal_explored_path_costs[
            start_node] < best_cost:
            path_intersection = start_explored[start_node] + [start_node] + goal_explored[start_node]
            best_cost = start_explored_path_costs[start_node] + goal_explored_path_costs[start_node]
        for n in graph.neighbors(start_node):
            if n not in start_explored_set or n not in start_frontier:
                g = graph.get_edge_weight(start_node, n) + start_cost
                if start_explored_path_costs.get(n, float('inf')) > g:
                    start_frontier.append((g, n))
                    start_explored[n] = start_explored[start_node] + [start_node]
                    start_explored_path_costs[n] = g

        # search backward
        goal_cost, goal_node = goal_frontier.pop()
        goal_explored_set.add(goal_node)
        if goal_node in start_explored.keys() and start_explored_path_costs[goal_node] + goal_explored_path_costs[
            goal_node] < best_cost:
            path_intersection = start_explored[goal_node] + [goal_node] + goal_explored[goal_node]
            best_cost = start_explored_path_costs[goal_node] + goal_explored_path_costs[goal_node]
        for n in graph.neighbors(goal_node):
            if n not in goal_explored_set or n not in goal_frontier:
                g = graph.get_edge_weight(n, goal_node) + goal_cost
                if goal_explored_path_costs.get(n, float('inf')) > g:
                    goal_frontier.append((g, n))
                    goal_explored[n] = [goal_node] + goal_explored[goal_node]
                    goal_explored_path_costs[n] = g

        # see if frontiers have intersected and return if so
        # Stopping Criterion:  min(forward) + min(reverse) > shortest_path_in_graph
        if start_frontier.peek() + goal_frontier.peek() >= best_cost:
            return path_intersection

    return list()


def bi_a_star_stopping_criterion(start_node, goal_node, start_explored_path_costs, goal_explored_path_costs, best_cost,
                                 graph, heuristic):
    if float(start_explored_path_costs[start_node] + goal_explored_path_costs[goal_node]) >= float(
            best_cost + heuristic(graph, start_node, goal_node)):
        return True

    return False


def bidirectional_a_star(graph, start, goal,
                         heuristic=euclidean_dist_heuristic):
    """
    Exercise 2: Bidirectional A*.

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """
    if goal_test(start, goal, graph):
        return list()

    if is_node_found(start, goal, graph):
        return list()

    start_frontier = PriorityQueue()
    goal_frontier = PriorityQueue()
    start_frontier.append((0, start))
    goal_frontier.append((0, goal))

    start_explored = {start: list()}
    goal_explored = {goal: list()}
    start_explored_path_costs = {start: 0}
    goal_explored_path_costs = {goal: 0}

    start_explored_sets = set()
    goal_explored_sets = set()

    path_intersection = list()
    best_cost = float('inf')  # best s-t path seen so far

    while start_frontier and goal_frontier:
        start_cost, start_node = start_frontier.pop()
        start_explored_sets.add(start_node)
        if start_node in goal_explored.keys() and start_explored_path_costs[start_node] + goal_explored_path_costs[
            start_node] < best_cost:
            best_cost = float(start_explored_path_costs[start_node] + goal_explored_path_costs[start_node])
            path_intersection = start_explored[start_node] + [start_node] + goal_explored[start_node]
        for n in graph.neighbors(start_node):
            if n not in start_explored_sets or n not in start_frontier:
                g = float(graph.get_edge_weight(start_node, n) + start_cost)
                if start_explored_path_costs.get(n, float('inf')) > g:
                    start_explored_path_costs[n] = g
                    start_frontier.append((g, n))
                    start_explored[n] = start_explored[start_node] + [start_node]

        # topf:  length of the path from s to top element of forward heap
        # topr: length of (reverse) path from t to top element of reverse heap
        start_cost, start_node = start_frontier.top()[-1]
        goal_cost, goal_node = goal_frontier.top()[-1]
        if bi_a_star_stopping_criterion(start_node, goal_node, start_explored_path_costs, goal_explored_path_costs,
                                        best_cost, graph, heuristic):
            return path_intersection

        goal_cost, goal_node = goal_frontier.pop()
        goal_explored_sets.add(goal_node)
        if goal_node in start_explored.keys() and start_explored_path_costs[goal_node] + goal_explored_path_costs[
            goal_node] < best_cost:
            best_cost = float(start_explored_path_costs[goal_node] + goal_explored_path_costs[goal_node])
            path_intersection = start_explored[goal_node] + [goal_node] + goal_explored[goal_node]
        for n in graph.neighbors(goal_node):
            if n not in goal_explored_sets or n not in goal_frontier:
                g = float(graph.get_edge_weight(n, goal_node) + goal_cost)
                if goal_explored_path_costs.get(n, float('inf')) > g:
                    goal_explored_path_costs[n] = g
                    goal_frontier.append((g, n))
                    goal_explored[n] = [goal_node] + goal_explored[goal_node]

        start_cost, start_node = start_frontier.top()[-1]
        goal_cost, goal_node = goal_frontier.top()[-1]

        # Use average - feasible and consistent
        # pif(v): estimate on dist(v,t)
        # pir(v): estimate on dist(s,v)
        # pf(v) = 0.5(pif(v) - pir(v)) + 0.5*pir(t)
        # pr(v) = 0.5(pir(v) - pif(v)) + 0.5*pif(s)
        # mu: best s-t path seen so far

        # Stopping criterion
        # topf + topr >= mu + pr(t)
        if bi_a_star_stopping_criterion(start_node, goal_node, start_explored_path_costs, goal_explored_path_costs,
                                        best_cost, graph, heuristic):
            return path_intersection

    return list()


def tridirectional_search(graph, goals):
    """
    Exercise 3: Tridirectional UCS Search

    See README.MD for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        goals (list): Key values for the 3 goals

    Returns:
        The best path as f1 list from one of the goal nodes (including both of
        the other goal nodes).
    """
    # return tridirectional_upgraded(graph, goals, heuristic=euclidean_dist_heuristic)
    # early terminate condition
    if len(goals) < 2:
        return list()

    # check for existence of goal that has been explored already
    for g in goals:
        if is_node_found(g, g, graph):
            return list()

    g1, g2, g3 = goals

    if goal_test(g1, g2, graph):
        return list()
    if goal_test(g2, g3, graph):
        return list()
    if goal_test(g1, g3, graph):
        return list()

    frontier_1 = PriorityQueue()
    frontier_2 = PriorityQueue()
    frontier_3 = PriorityQueue()
    frontier_1.append((0, g1))
    frontier_2.append((0, g2))
    frontier_3.append((0, g3))

    explored_1 = dict()
    explored_1[g1] = list()

    explored_2 = dict()
    explored_2[g2] = list()

    explored_3 = dict()
    explored_3[g3] = list()

    explored_path_costs_1 = dict()
    explored_path_costs_1[g1] = 0

    explored_path_costs2 = dict()
    explored_path_costs2[g2] = 0

    explored_path_costs3 = dict()
    explored_path_costs3[g3] = 0

    explored_set_1 = set()
    explored_set_2 = set()
    explored_set_3 = set()

    best_cost_12 = float('inf')
    best_cost_13 = float('inf')
    best_cost_23 = float('inf')

    path_intersection_12 = list()
    path_intersection_13 = list()
    path_intersection_23 = list()

    while frontier_1 and frontier_2 and frontier_3:

        # explore frontier 1
        cost_1, node_1 = frontier_1.pop()
        explored_set_1.add(cost_1)

        # check optimal node conditions
        if cost_1 < best_cost_12:
            if node_1 in explored_2.keys() and float(explored_path_costs_1[node_1] + explored_path_costs2[
                node_1]) < best_cost_12:
                # store the new best path found
                best_cost_12 = float(explored_path_costs_1[node_1] + explored_path_costs2[node_1])
                path_intersection_12 = explored_1[node_1] + [node_1] + explored_2[node_1][::-1]
            # find its remaining neighbors and add to frontier
            for n in graph.neighbors(node_1):
                if n not in explored_set_1 or n not in frontier_1:
                    g = graph.get_edge_weight(node_1, n) + cost_1
                    if explored_path_costs_1.get(n, float('inf')) > g:
                        frontier_1.append((g, n))
                        explored_1[n] = explored_1[node_1] + [node_1]
                        explored_path_costs_1[n] = g

        # check optimal node conditions
        if cost_1 < best_cost_13:
            if node_1 in explored_3.keys() and float(explored_path_costs_1[node_1] + explored_path_costs3[
                node_1]) < best_cost_13:
                # store the new best path found
                best_cost_13 = float(explored_path_costs_1[node_1] + explored_path_costs3[node_1])
                path_intersection_13 = explored_1[node_1] + [node_1] + explored_3[node_1][::-1]
            # find its remaining neighbors and add to frontier
            for n in graph.neighbors(node_1):
                if n not in explored_set_1 or n not in frontier_1:
                    g = graph.get_edge_weight(node_1, n) + cost_1
                    if explored_path_costs_1.get(n, float('inf')) > g:
                        frontier_1.append((g, n))
                        explored_1[n] = explored_1[node_1] + [node_1]
                        explored_path_costs_1[n] = g

        # look for and return intersection between 3 paths
        f1 = frontier_1.top()[0]
        f2 = frontier_2.top()[0]
        f3 = frontier_3.top()[0]
        f12 = float(f1 + f2)
        f13 = float(f1 + f3)
        f23 = float(f2 + f3)
        if min(f12, f23) >= best_cost_12:
            if min(f3, f23) >= best_cost_13:
                return tri_star_stopping_criterion(path_intersection_12, path_intersection_23, path_intersection_13, best_cost_12,
                                                   best_cost_23, best_cost_13)

        # explore frontier 2
        cost_2, node_2 = frontier_2.pop()
        explored_set_2.add(node_2)

        # check optimal node conditions
        if cost_2 < best_cost_12:
            if node_2 in explored_1.keys() and float(explored_path_costs_1[node_2] + explored_path_costs2[
                node_2]) < best_cost_12:
                # store the new best path found
                best_cost_12 = float(explored_path_costs_1[node_2] + explored_path_costs2[node_2])
                path_intersection_12 = explored_1[node_2] + [node_2] + explored_2[node_2][::-1]
            # find its remaining neighbors and add to frontier
            for n in graph.neighbors(node_2):
                if n not in explored_set_2 or n not in frontier_2:
                    g = graph.get_edge_weight(node_2, n) + cost_2
                    if explored_path_costs2.get(n, float('inf')) > g:
                        frontier_2.append((g, n))
                        explored_2[n] = explored_2[node_2] + [node_2]
                        explored_path_costs2[n] = g
        # check optimal node conditions
        if cost_2 < best_cost_23:
            if node_2 in explored_3.keys() and float(explored_path_costs2[node_2] + explored_path_costs3[
                node_2]) < best_cost_23:
                # store the new best path found
                best_cost_23 = float(explored_path_costs2[node_2] + explored_path_costs3[node_2])
                path_intersection_23 = explored_2[node_2] + [node_2] + explored_3[node_2][::-1]
            # find its remaining neighbors and add to frontier
            for n in graph.neighbors(node_2):
                if n not in explored_set_2 or n not in frontier_2:
                    g = graph.get_edge_weight(node_2, n) + cost_2
                    if explored_path_costs2.get(n, float('inf')) > g:
                        frontier_2.append((g, n))
                        explored_2[n] = explored_2[node_2] + [node_2]
                        explored_path_costs2[n] = g

        # look for and return intersection between 3 paths
        f1 = frontier_1.top()[0]
        f2 = frontier_2.top()[0]
        f3 = frontier_3.top()[0]
        f12 = float(f1 + f2)
        f13 = float(f1 + f3)
        f23 = float(f2 + f3)
        if min(f13, f12) >= best_cost_12:
            if min(f13, f23) >= best_cost_23:
                return tri_star_stopping_criterion(path_intersection_12, path_intersection_23, path_intersection_13, best_cost_12,
                                                   best_cost_23, best_cost_13)

        # explore frontier 3
        cost_3, node_3 = frontier_3.pop()
        explored_set_3.add(node_3)

        # check optimal node conditions
        if cost_3 < best_cost_13:
            if node_3 in explored_1.keys() and float(explored_path_costs_1[node_3] + explored_path_costs3[
                node_3]) < best_cost_13:
                # store the new best path found
                best_cost_13 = float(explored_path_costs_1[node_3] + explored_path_costs3[node_3])
                path_intersection_13 = explored_1[node_3] + [node_3] + explored_3[node_3][::-1]
            # find its remaining neighbors and add to frontier
            for n in graph.neighbors(node_3):
                if n not in explored_set_3 or n not in frontier_3:
                    g = graph.get_edge_weight(node_3, n) + cost_3
                    if explored_path_costs3.get(n, float('inf')) > g:
                        frontier_3.append((g, n))
                        explored_3[n] = explored_3[node_3] + [node_3]
                        explored_path_costs3[n] = g

        # check optimal node conditions
        if cost_3 < best_cost_23:
            if node_3 in explored_2.keys() and float(explored_path_costs2[node_3] + explored_path_costs3[
                node_3]) < best_cost_23:
                # store the new best path found
                best_cost_23 = float(explored_path_costs2[node_3] + explored_path_costs3[node_3])
                path_intersection_23 = explored_2[node_3] + [node_3] + explored_3[node_3][::-1]
            # find its remaining neighbors and add to frontier
            for n in graph.neighbors(node_3):
                if n not in explored_set_3 or n not in frontier_3:
                    g = graph.get_edge_weight(node_3, n) + cost_3
                    if explored_path_costs3.get(n, float('inf')) > g:
                        frontier_3.append((g, n))
                        explored_3[n] = explored_3[node_3] + [node_3]
                        explored_path_costs3[n] = g

        # look for and return intersection between 3 paths
        f1 = frontier_1.top()[0]
        f2 = frontier_2.top()[0]
        f3 = frontier_3.top()[0]
        f12 = float(f1 + f2)
        f13 = float(f1 + f3)
        f23 = float(f2 + f3)
        if min(f12, f13) >= best_cost_13:
            if min(f12, f23) >= best_cost_23:
                return tri_star_stopping_criterion(path_intersection_12, path_intersection_23, path_intersection_13, best_cost_12,
                                                   best_cost_23, best_cost_13)

    return list()


def tri_star_stopping_criterion(path_intersection_12, path_intersection_23, path_intersection_13, best_cost_12, best_cost_23,
                                best_cost_13):
    """
    Check for optimal costs and return intersection that contains tri-star paths.
    """

    if best_cost_12 < best_cost_23:
        if best_cost_13 < best_cost_23:
            if path_intersection_13 in path_intersection_12:
                return path_intersection_12
            elif path_intersection_12 in path_intersection_13:
                return path_intersection_13
            else:
                return path_intersection_12[::-1] + path_intersection_13[1:]

    if best_cost_12 < best_cost_13:
        if best_cost_23 < best_cost_13:
            if path_intersection_12 in path_intersection_23:
                return path_intersection_23
            elif path_intersection_23 in path_intersection_12:
                return path_intersection_12
            else:
                return path_intersection_12 + path_intersection_23[1:]

    if best_cost_13 < best_cost_12:
        if best_cost_23 < best_cost_12:
            if path_intersection_13 in path_intersection_23:
                return path_intersection_23
            elif path_intersection_23 in path_intersection_13:
                return path_intersection_13
            else:
                return path_intersection_13[:-1] + path_intersection_23[::-1]

    return list()


def tridirectional_upgraded(graph, goals, heuristic=euclidean_dist_heuristic):
    """
    Exercise 4: Upgraded Tridirectional Search

    See README.MD for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        goals (list): Key values for the 3 goals
        heuristic: Function to determine distance heuristic.
            Default: euclidean_dist_heuristic.

    Returns:
        The best path as a list from one of the goal nodes (including both of
        the other goal nodes).
    """

    # early terminate condition
    if len(goals) < 2:
        return list()

    # check for existence of goal that has been explored already
    for g in goals:
        if is_node_found(g, g, graph):
            return list()

    return tridirectional_search(graph, goals)

def return_your_name():
    """Return your name from this function"""
    return "Brandon Sheffield"


def custom_heuristic(graph, v, goal):
    """
       Feel free to use this method to try and work with different heuristics and come up with a better search algorithm.
       Args:
           graph (ExplorableGraph): Undirected graph to search.
           v (str): Key for the node to calculate from.
           goal (str): Key for the end node to calculate to.
       Returns:
           Custom heuristic distance between `v` node and `goal` node
       """


pass


# Extra Credit: Your best search method for the race
def custom_search(graph, start, goal, data=None):
    """
    Race!: Implement your best search algorithm here to compete against the
    other student agents.

    If you implement this function and submit your code to Gradescope, you'll be
    registered for the Race!

    See README.md for exercise description.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        start (str): Key for the start node.
        goal (str): Key for the end node.
        data :  Data used in the custom search.
            Will be passed your data from load_data(graph).
            Default: None.

    Returns:
        The best path as a list from the start and goal nodes (including both).
    """

    # TODO: finish this function!
    raise NotImplementedError


def load_data(graph, time_left):
    """
    Feel free to implement this method. We'll call it only once 
    at the beginning of the Race, and we'll pass the output to your custom_search function.
    graph: a networkx graph
    time_left: function you can call to keep track of your remaining time.
        usage: time_left() returns the time left in milliseconds.
        the max time will be 10 minutes.

    * To get a list of nodes, use graph.nodes()
    * To get node neighbors, use graph.neighbors(node)
    * To get edge weight, use graph.get_edge_weight(node1, node2)
    """

    # nodes = graph.nodes()
    return None


def haversine_dist_heuristic(graph, v, goal):
    """
    Note: This provided heuristic is for the Atlanta race.

    Args:
        graph (ExplorableGraph): Undirected graph to search.
        v (str): Key for the node to calculate from.
        goal (str): Key for the end node to calculate to.

    Returns:
        Haversine distance between `v` node and `goal` node
    """

    # Load latitude and longitude coordinates in radians:
    vLatLong = (math.radians(graph.nodes[v]["pos"][0]), math.radians(graph.nodes[v]["pos"][1]))
    goalLatLong = (math.radians(graph.nodes[goal]["pos"][0]), math.radians(graph.nodes[goal]["pos"][1]))

    # Now we want to execute portions of the formula:
    constOutFront = 2 * 6371  # Radius of Earth is 6,371 kilometers
    term1InSqrt = (math.sin((goalLatLong[0] - vLatLong[0]) / 2)) ** 2  # First term inside sqrt
    term2InSqrt = math.cos(vLatLong[0]) * math.cos(goalLatLong[0]) * (
            (math.sin((goalLatLong[1] - vLatLong[1]) / 2)) ** 2)  # Second term
    return constOutFront * math.asin(math.sqrt(term1InSqrt + term2InSqrt))  # Straight application of formula
